package com.example.Validador;

import java.util.Scanner;

public class Rut {

    public static void main(String[] args){
        System.out.println(crear());
        validado();
    }

    public static String entradaString(){
        Scanner teclado = new Scanner (System.in);
        System.out.println("Ingrese su rut aquí: ");
        return teclado.nextLine();
    }

    public static boolean validar(String rut){
        return rut.matches("^[1-9]\\d?\\.?\\d{3}\\.?\\d{3}-?[\\dkK]$");
    }

    public static boolean validarVerificador(String rut){
        int j = 2;
        int verificador = 0;

        for(int i = rut.length() - 2; i > -1; --i) {
            if (j > 7) {
                j = 2;
            }

            verificador += Integer.parseInt(String.valueOf(rut.charAt(i))) * j;
            ++j;

        }

        verificador = 11 - Math.abs(verificador / 11 * 11 - verificador);
        if (rut.charAt(rut.length() - 1) == 'k' && verificador == 10) {
            return true;
        } else if (rut.charAt(rut.length() - 1) == '0' && verificador == 11) {
            return true;
        } else if (rut.charAt(rut.length() - 1) == 'k' && verificador != 10) {
            return false;
        } else {
            return verificador == Integer.parseInt(String.valueOf(rut.charAt(rut.length() - 1)));
        }

    }

    public static String crear() {
        String rut = entradaString();
        if (!validar(rut)) {
            invalidar();
            rut = crear();
        }

        rut = rut.replace(".", "");
        rut = rut.replace("-", "");
        if (!validarVerificador(rut.toLowerCase())) {
            invalidar();
            rut = crear();
        }

        return rut;
    }

    public static void invalidar() {
        System.out.println("El rut ingresado no es válido");
    }

    public static void validado() {
        System.out.println("El rut ingresado es válido");
    }

}
